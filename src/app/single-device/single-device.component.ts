import { DeviceService } from './../services/device.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-device',
  templateUrl: './single-device.component.html',
  styleUrls: ['./single-device.component.scss']
})
export class SingleDeviceComponent implements OnInit {
  name: string = 'Device';
  status: string = 'Status';

  constructor(private appareilService: DeviceService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.name = this.appareilService.getDeviceById(+id).name;
    this.status = this.appareilService.getDeviceById(+id).status;
  }

}
