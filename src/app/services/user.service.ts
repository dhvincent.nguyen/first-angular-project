import { User } from "../model/user.models";
import { Subject } from "rxjs";

export class UserService {
  private users: User[] = [
    new User('Vincent', 'Nguyen', 'dhvincent.nguyen@gmail.com', 
    'beer', ['dev', 'drink coffee'])
  ];
  userSubject = new Subject<User[]>();

  emitUsers() {
    this.userSubject.next(this.users.slice());
  }

  addUser(user: User) {
    this.users.push(user);
    this.emitUsers();
  }
}