import { Injectable } from '@angular/core';
import { Device } from "../device";
import { Subject } from "rxjs";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DeviceService {
    devicesSubject = new Subject<Device[]>()
    private devices: Device[] = []

	constructor(private httpClient: HttpClient) { }    

    saveDevicesToServer() {
        this.httpClient
          .put('https://first-angular-project-a72e1.firebaseio.com/devices.json', this.devices)
          .subscribe(
            () => {
              console.log('Saving devices done !');
            },
            (error) => {
              console.log('Error ! : ' + error);
            }
          );
    }

    getDevicesFromServer() {
        this.httpClient
          .get<Device[]>('https://first-angular-project-a72e1.firebaseio.com/devices.json')
          .subscribe(
            (response) => {
                console.log(response)
                this.devices = response;
                this.emitDeviceSubject();
            },
            (error) => {
              console.log('Error ! : ' + error);
            }
          );
    }

    emitDeviceSubject() {
        this.devicesSubject.next(this.devices.slice())
    }

    addDevice(name: string, status: string) {
        const newDevice = new Device(0, name,status)
        newDevice.id = this.devices[(this.devices.length - 1)].id + 1;
        this.devices.push(newDevice);
        this.saveDevicesToServer()
        this.emitDeviceSubject();
    }

    deleteDevice(index: number) {
        this.devices.splice(index, 1)
        this.saveDevicesToServer()
        this.emitDeviceSubject()
    }
    
    getDeviceById(id: number) {
        const device = this.devices.find(
          (s) => {
            return s.id === id;
          }
        );
        return device;
    }

    updateDevice(id: number, name: string, status: string) {
        var device = this.getDeviceById(id)
        device.name = name
        device.status = status
        this.saveDevicesToServer()
    }

    switchOnAll() {
        for(let device of this.devices) {
            device.status = 'On';
        }
        this.emitDeviceSubject()
    }
    
    switchOffAll() {
        for(let device of this.devices) {
            device.status = 'Off';
        }
        this.emitDeviceSubject()
    }

    switchOnOne(i: number) {
        this.devices[i].status = 'On';
        this.emitDeviceSubject()
    }
    
    switchOffOne(i: number) {
        this.devices[i].status = 'Off';
        this.emitDeviceSubject()
    }
}