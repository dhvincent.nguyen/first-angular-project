import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { DeviceService } from '../services/device.service';
import { Subscription } from 'rxjs';
import { Device } from '../device';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent implements OnInit, OnDestroy {
  isAuth = false
  devices: Device[] = []
  deviceSubscription: Subscription;
  
  constructor(private deviceService: DeviceService) {
    setTimeout(
      () => {
        this.isAuth = true
      }, 2000
    );
  }

  ngOnInit() {
    this.deviceService.getDevicesFromServer()
    this.deviceSubscription = this.deviceService.devicesSubject.subscribe(
      (devices: any[]) => {
        this.devices = devices
      }
    )
    this.deviceService.emitDeviceSubject()
  }

  ngOnDestroy(): void {
    this.deviceSubscription.unsubscribe()
  }

  onSwitchOnAll() {
    this.deviceService.switchOnAll()
    this.deviceService.saveDevicesToServer()
  }

  onSwitchOffAll() {
    if(confirm('Are you sure to switch off all your devices?')) {
      this.deviceService.switchOffAll()
      this.deviceService.saveDevicesToServer()
    } else {
      return null;
    }
  }

  onSave() {
    this.deviceService.saveDevicesToServer()
  }

  onFetch() {
    this.deviceService.getDevicesFromServer();
  }
}
