import { Observable, Subscription, interval, timer } from 'rxjs';
import { DeviceService } from './services/device.service';
import { Device } from './device';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { resolve } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  //seconds: number
  //counterSubscription: Subscription
  currentDateTime: Date

  ngOnInit() {
    //const counter = interval(1000)
    this.currentDateTime = new Date()
    // this.counterSubscription = counter.subscribe(
    //   (value) => {
    //     this.seconds = value;
    //     this.currentDateTime.setSeconds(this.currentDateTime.getSeconds() + 1)
    //   },
    //   (error) => {
    //     console.log('Uh-oh, an error occurred! : ' + error);
    //   },
    //   () => {
    //     console.log('Observable complete!');
    //   }
    // );
  }

  ngOnDestroy() {
    //this.counterSubscription.unsubscribe()
  }

}
